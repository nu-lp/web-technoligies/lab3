import { Request, Response } from 'express';
import Product, { ProductDto } from './product.model';

const addProduct = (req: Request, res: Response) => {
  const product = req.body as ProductDto;
  const newProduct = new Product({
    name: product.name,
    description: product.description,
    type: product.type
  })
  console.log('create new product', newProduct);

  return newProduct.save()
    .then((saved) => {
      saved.toObject({})
      const response: ProductDto = {
        id: saved.id,
        name: saved.name,
        description: saved.description,
        type: saved.type,
        createdAt: saved.createdAt
      };
      return res.status(201).json(response);
    })
    .catch((error) => {
      return res.status(500).json({
        message: error.message,
        error: error
      });
    });
};


const getAllProducts = (req: Request, res: Response) => {
  console.log('get all products');
  return Product.find()
    .exec()
    .then((docs) => {
      const products: ProductDto[] = docs.map(doc => {
        return {
          id: doc.id,
          name: doc.name,
          description: doc.description,
          type: doc.type,
          createdAt: doc.createdAt
        };
      });
      return res.status(200).json(products);
    })
    .catch((error) => {
      return res.status(500).json({
        message: error.message,
        error: error
      });
    });
};

const deleteProduct = (req: Request, res: Response) => {
  const id = req.params['id'];
  console.log('delete product with id', id);
  return Product.remove({_id: id})
    .then(() => {
      return res.status(204).json();
    })
    .catch((error) => {
      return res.status(500).json({
        message: error.message,
        error: error
      });
    });
};

export default {addProduct, getAllProducts, deleteProduct};
