import mongoose, { Document, Schema } from 'mongoose';

export enum ProductType {
  TECH = 'TECH',
  FOOD = 'FOOD',
  CLOTHES = 'CLOTHES',
  SERVICE = 'SERVICE',
}

export interface ProductDto {
  id?: string;
  name: string;
  description: string;
  type: ProductType;
  createdAt?: Date;
}

interface Product extends Document {
  name: string;
  description: string;
  type: ProductType;
  createdAt: Date;
}

const ProductSchema: Schema = new Schema(
  {
    name: {
      type: String,
      unique: true,
      required: true
    },
    description: {
      type: String,
      required: true
    },
    type: {
      type: String,
      required: true
    }
  },
  {
    versionKey: false,
    timestamps: true
  }
);

export default mongoose.model<Product>('Product', ProductSchema);
