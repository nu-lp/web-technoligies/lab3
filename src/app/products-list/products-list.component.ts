import { Component, OnDestroy, OnInit } from '@angular/core';
import { ProductModel } from '../product/product.model';
import { ProductService } from '../product/product.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss']
})
export class ProductsListComponent implements OnInit, OnDestroy {
  private _subscription: Subscription | undefined;

  products: ProductModel[] = [];

  constructor(private service: ProductService) {
  }

  ngOnInit(): void {
    this._subscription = this.service.products.subscribe(products => this.products = products);
    this.service.getAllProducts();
  }

  ngOnDestroy(): void {
    this._subscription?.unsubscribe();
  }

  deleteProduct($event: string) {
    this.service.deleteProduct($event);
  }
}
