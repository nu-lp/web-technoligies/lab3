import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ProductModel } from './product.model';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  @Input() product: ProductModel = {} as ProductModel;
  @Output() deleteProduct = new EventEmitter<string>()

  constructor() {
  }

  ngOnInit(): void {
  }

  emitRemoveEvent() {
    if (confirm('Are you sure you want to delete ' + this.product.name)) {
      this.deleteProduct.emit(this.product.id);
    }
  }
}
