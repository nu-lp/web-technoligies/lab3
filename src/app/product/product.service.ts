import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { ProductModel } from './product.model';
import { environment as env } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private readonly _products = new BehaviorSubject<ProductModel[]>([]);

  get products(): Observable<ProductModel[]> {
    return this._products.asObservable();
  }

  get productsSnapshot(): ProductModel[] {
    return this._products.getValue();
  }

  constructor(private http: HttpClient) {
  }

  getAllProducts() {
    this.http.get<ProductModel[]>(`${env.apiUrl}/products`)
      .subscribe(products => this._products.next(products));
  }

  addProduct(newProduct: ProductModel) {
    this.http.post<ProductModel>(`${env.apiUrl}/products`, newProduct)
      .subscribe(product => this._products.next(this.productsSnapshot.concat(product)));
  }

  deleteProduct(id: string) {
    this.http.delete(`${env.apiUrl}/products/${id}`)
      .subscribe(() => this._products.next(this.productsSnapshot.filter(p => p.id !== id)));
  }
}
