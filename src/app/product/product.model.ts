export enum ProductType {
  TECH = 'TECH',
  FOOD = 'FOOD',
  CLOTHES = 'CLOTHES',
  SERVICE = 'SERVICE',
}

export interface ProductModel {
  id?: string;
  name: string;
  description: string;
  type: ProductType;
  createdAt?: Date;
}
