import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ProductType } from '../product/product.model';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-new-product-modal',
  templateUrl: './new-product-modal.component.html',
  styleUrls: ['./new-product-modal.component.scss']
})
export class NewProductModalComponent implements OnInit {
  readonly ProductTypes = Object.keys(ProductType);
  form: FormGroup = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(4)]),
    description: new FormControl('', [Validators.required, Validators.minLength(10)]),
    type: new FormControl('', [Validators.required])
  });

  getError(errorCode: string, path: string) {
    return this.form.getError(errorCode, path);
  }

  constructor(private dialogRef: MatDialogRef<NewProductModalComponent>) {
  }

  ngOnInit(): void {
  }

  onSubmit() {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return;
    }
    console.log('new product', this.form.value);
    this.dialogRef.close(this.form.value);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
