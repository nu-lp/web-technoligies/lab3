import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { NewProductModalComponent } from '../new-product-modal/new-product-modal.component';
import { ProductService } from '../product/product.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private dialog: MatDialog,
              private productService: ProductService) {
  }

  ngOnInit(): void {
  }

  addUserDialog() {
    const dialogRef = this.dialog.open(NewProductModalComponent, {
      width: '640px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('Add user dialog was closed', result);
      if (result) {
        this.productService.addProduct(result);
      }
    });
  }
}
